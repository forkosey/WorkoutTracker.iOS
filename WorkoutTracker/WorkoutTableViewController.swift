//
//  WorkoutTableViewController.swift
//  WorkoutTracker
//
//  Created by Marc-Antoine Giguère on 2018-04-23.
//  Copyright © 2018 Marc-Antoine Giguère. All rights reserved.
//

import UIKit
import os.log
import Floaty

var GlobalUnitType = [(String, String)]()

class WorkoutTableViewController: UITableViewController {

    //MARK: Properties
    var workouts = [Workout]()
    
    
    //MARK: Private Methods
    private func loadSampleWorkouts() {
        guard let wo1 = Workout.init(name: "Chest", desc: "Description", exerciseArray: [
            Exercise.init(name: "Bench press", desc: "Description", setArray: [WOSet.init(value: 135, repetition: 12, valueUnitType: "lbs")!,
                                                                               WOSet.init(value: 135, repetition: 12, valueUnitType: "lbs")!])!,
            Exercise.init(name: "Incline bench press", desc: "Description", setArray: [WOSet.init(value: 135, repetition: 12, valueUnitType: "lbs")!,
                                                                                       WOSet.init(value: 135, repetition: 12, valueUnitType: "lbs")!])!], isFavorite: true)
            else {
                fatalError("Unable to instantiate wo1")
        }
        
        guard let wo2 = Workout.init(name: "Back", desc: "Description", exerciseArray: [
            Exercise.init(name: "Lat pulldown", desc: "Description", setArray: [WOSet.init(value: 110, repetition: 12, valueUnitType: "lbs")!,
                                                                                WOSet.init(value: 110, repetition: 12, valueUnitType: "lbs")!])!,
            Exercise.init(name: "Deadlift", desc: "Description", setArray: [WOSet.init(value: 300, repetition: 5, valueUnitType: "lbs")!,
                                                                            WOSet.init(value: 300, repetition: 5, valueUnitType: "lbs")!])!], isFavorite: true)
            else {
                fatalError("Unable to instantiate wo2")
        }
        
        guard let wo3 = Workout.init(name: "Legs", desc: "Description", exerciseArray: [
            Exercise.init(name: "Squats", desc: "Description", setArray: [WOSet.init(value: 185, repetition: 12, valueUnitType: "lbs")!,
                                                                          WOSet.init(value: 185, repetition: 12, valueUnitType: "lbs")!])!,
            Exercise.init(name: "Lunge", desc: "Description", setArray: [WOSet.init(value: 70, repetition: 12, valueUnitType: "lbs")!,
                                                                         WOSet.init(value: 70, repetition: 12, valueUnitType: "lbs")!])!], isFavorite: false)
            else {
                fatalError("Unable to instantiate wo3")
        }
        
        workouts += [wo1, wo2, wo3]
    }
    
    private func saveWorkouts() {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(workouts, toFile: Workout.ArchiveURL.path)
        if isSuccessfulSave {
            os_log("Workouts successfully saved.", log: OSLog.default, type: .debug)
        } else {
            os_log("Failed to save workout...", log: OSLog.default, type: .error)
        }
    }
    
    private func loadWorkouts() -> [Workout]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: Workout.ArchiveURL.path) as? [Workout]
    }
    
    private func getFavoriteWorkout() -> [Workout] {
        var favoriteList = [Workout]()
        for workout in workouts {
            if workout.isFavorite {
                favoriteList.append(workout)
            }
        }
        return favoriteList
    }
    
    private func getNotFavoriteWorkout() -> [Workout] {
        var notFavoriteList = [Workout]()
        for workout in workouts {
            if !workout.isFavorite {
                notFavoriteList.append(workout)
            }
        }
        return notFavoriteList
    }
    
    
    //MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.leftBarButtonItem = editButtonItem
        
        if let savedWorkouts = loadWorkouts() {
            workouts += savedWorkouts
        } else {
            //loadSampleWorkouts()
        }
        
        Floaty.global.button.addItem("Create new Workout", icon: UIImage(named: "flex25")!, handler: { _ in
            self.performSegue(withIdentifier: "AddItem", sender: self)
        })
        Floaty.global.button.addItem("Settings", icon: UIImage(named: "settings25")!, handler: { _ in
            if let url = URL(string:UIApplicationOpenSettingsURLString) {
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            }
        })
        Floaty.global.show()
        
        UserDefaults.standard.register(defaults: [String : Any]())
        let userDefaults = UserDefaults.standard
        
        let unitPreference = userDefaults.value(forKey: "units_preference") as? String ?? ""
        
        switch unitPreference {
        case "0":
            GlobalUnitType = [("Weight", "lbs"), ("Weight", "kg"),
                              ("Distance", "m"), ("Distance", "km"),
                              ("Time", "sec"), ("Time", "min")]
            
        case "1":
            GlobalUnitType = [("Weight", "kg"), ("Weight", "lbs"),
                              ("Distance", "yd"), ("Distance", "mi"),
                              ("Time", "sec"), ("Time", "min")]
            
        case "":
            GlobalUnitType = [("Weight", "lbs"), ("Weight", "kg"),
                              ("Distance", "m"), ("Distance", "km"),
                              ("Time", "sec"), ("Time", "min")]
        
        default:
            fatalError("units_preference wasn't a string.")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        Floaty.global.show()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return workouts.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "WorkoutTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? WorkoutTableViewCell else {
            fatalError("The dequeued cell is not an instance of WorkoutTableViewCell.")
        }
        
        let wo = workouts[indexPath.row]
        
        cell.workoutNameLabel.text = wo.name
        cell.exerciseNumberLabel.text = String(wo.exerciseArray.count) + " exercise(s)"
        cell.favoriteButton.tag = indexPath.row
        if wo.isFavorite {
            cell.favoriteButton.setImage(UIImage(named: "filledStar25"), for: .normal)
        } else {
            cell.favoriteButton.setImage(UIImage(named: "emptyStar25"), for: .normal)
        }

        return cell
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            workouts.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            saveWorkouts()
        } else if editingStyle == .insert {
        }    
    }
    
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        let movedWorkout = workouts[fromIndexPath.row]
        workouts.remove(at: fromIndexPath.row)
        workouts.insert(movedWorkout, at: to.row)
        saveWorkouts()
    }
 
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
 

    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch(segue.identifier ?? "") {
        case "AddItem":
            os_log("Adding a new workout.", log: OSLog.default, type: .debug)
            
        case "ShowDetail":
            guard let WorkoutDetailViewController = segue.destination as? WorkoutViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            
            guard let selectedWorkoutCell = sender as? WorkoutTableViewCell else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            
            guard let indexPath = tableView.indexPath(for: selectedWorkoutCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            
            let selectedWorkout = workouts[indexPath.row]
            WorkoutDetailViewController.workout = selectedWorkout
            
        case "HomeToSettings":
            os_log("Going to settings", log: OSLog.default, type: .debug)
            
        default:
            fatalError("Unexpected Segue Identifier; \(String(describing: segue.identifier))")
        }
    }
    
    
    //MARK: Actions
    @IBAction func unwindToWorkoutList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? WorkoutViewController, let workout = sourceViewController.workout {
            
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                // Update an existing workout.
                workouts[selectedIndexPath.row] = workout
                tableView.reloadRows(at: [selectedIndexPath], with: .none)
            } else {
                // Add a new workout.
                let newIndexPath = IndexPath(row: workouts.count, section: 0)
                
                workouts.append(workout)
                tableView.insertRows(at: [newIndexPath], with: .automatic)
            }
            saveWorkouts()
        }
    }
    
    @IBAction func makeFavoriteButton(_ sender: UIButton) {
        workouts[sender.tag].isFavorite = !workouts[sender.tag].isFavorite
        saveWorkouts()
        tableView.reloadData()
    }
    
    
}
