//
//  WorkoutTableViewCell.swift
//  WorkoutTracker
//
//  Created by Marc-Antoine Giguère on 2018-04-23.
//  Copyright © 2018 Marc-Antoine Giguère. All rights reserved.
//

import UIKit

class WorkoutTableViewCell: UITableViewCell {

    //MARK: Properties
    @IBOutlet weak var workoutNameLabel: UILabel!
    @IBOutlet weak var exerciseNumberLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
