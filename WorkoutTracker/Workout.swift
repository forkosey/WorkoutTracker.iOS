//
//  Workout.swift
//  WorkoutTracker
//
//  Created by Marc-Antoine Giguère on 2018-04-19.
//  Copyright © 2018 Marc-Antoine Giguère. All rights reserved.
//

import UIKit
import os.log

class Workout: NSObject, NSCoding {
    
    //MARK: Properties
    var name: String
    var desc: String?
    var exerciseArray: [Exercise]
    var isFavorite: Bool
    
    
    //MARK: Archiving Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("workouts")
    
    
    //MARK: Types
    struct PropertyKey {
        static let name = "name"
        static let desc = "desc"
        static let exerciseArray = "exerciseArray"
        static let isFavorite = "isFavorite"
    }
    
    
    //MARK: Initialization
    init?(name: String, desc: String?, exerciseArray: [Exercise], isFavorite: Bool) {
        guard !name.isEmpty else {
            return nil
        }
        
        self.name = name
        self.desc = desc
        self.exerciseArray = exerciseArray
        self.isFavorite = isFavorite
        super.init()
    }
    
    
    //MARK: NSCoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: PropertyKey.name)
        aCoder.encode(desc, forKey: PropertyKey.desc)
        aCoder.encode(exerciseArray, forKey: PropertyKey.exerciseArray)
        aCoder.encode(isFavorite, forKey: PropertyKey.isFavorite)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        guard let name = aDecoder.decodeObject(forKey: PropertyKey.name) as? String else {
            os_log("Unable to decode the name for a Workout object.", log: OSLog.default, type: .debug)
            return nil
        }
        guard let desc = aDecoder.decodeObject(forKey: PropertyKey.desc) as? String else {
            os_log("Unable to decode the desc for a Workout object.", log: OSLog.default, type: .debug)
            return nil
        }
        guard let exerciseArray = aDecoder.decodeObject(forKey: PropertyKey.exerciseArray) as? [Exercise] else {
            os_log("Unable to decode the exerciseArray for a Workout object.", log: OSLog.default, type: .debug)
            return nil
        }
        let isFavorite = aDecoder.decodeBool(forKey: PropertyKey.isFavorite)
        
        self.init(name: name, desc: desc, exerciseArray: exerciseArray, isFavorite: isFavorite)
    }
    
}
