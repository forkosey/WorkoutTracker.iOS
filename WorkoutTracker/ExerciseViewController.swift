//
//  ExerciseViewController.swift
//  WorkoutTracker
//
//  Created by Marc-Antoine Giguère on 2018-04-23.
//  Copyright © 2018 Marc-Antoine Giguère. All rights reserved.
//

import UIKit
import os.log
import Floaty

class ExerciseViewController: UIViewController, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate {

    //MARK: Properties
    @IBOutlet weak var exerciseNameTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var addSetButton: UIBarButtonItem!
    @IBOutlet weak var setTableView: UITableView!
    
    var exercise: Exercise?
    var setArray = [WOSet]()
    var workoutName: String?
    
    
    //MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()
        
        exerciseNameTextField.delegate = self
        
        setTableView.delegate = self
        setTableView.dataSource = self
        
        if let exercise = exercise {
            exerciseNameTextField.text = exercise.name
            descriptionTextView.text = exercise.desc
            setArray = exercise.setArray
        }
        if let workoutName = workoutName {
            self.title = workoutName
        }
        
        updateSaveButtonState()
        addTextViewBorder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        Floaty.global.hide()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Private Methods
    private func clearSelectedCellIfOneIs(animated: Bool) {
        if let selectedIndexPath = setTableView.indexPathForSelectedRow {
            setTableView.deselectRow(at: selectedIndexPath, animated: animated)
        }
    }
    
    
    //MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Hide the keyboard.
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        saveButton.isEnabled = false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateSaveButtonState()
        navigationItem.title = textField.text
    }
    
    
    //MARK: Private Methods
    private func updateSaveButtonState() {
        // Disable the Save button if the text field is empty.
        let text = exerciseNameTextField.text ?? ""
        saveButton.isEnabled = !text.isEmpty
    }
    
    private func addTextViewBorder() {
        descriptionTextView.layer.borderWidth = 0.5
        descriptionTextView.layer.borderColor = UIColor.lightGray.cgColor
        descriptionTextView.layer.cornerRadius = 8.0
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if let button = sender as? UIBarButtonItem, button === saveButton {
            let name = exerciseNameTextField.text ?? ""
            var description = descriptionTextView.text
            
            if description == "Enter description" {
                description = ""
            }
            
            exercise = Exercise.init(name: name, desc: description, setArray: setArray)
        } else {
            switch(segue.identifier ?? "") {
            case "AddItem":
                clearSelectedCellIfOneIs(animated: false)
                os_log("Adding a new set.", log: OSLog.default, type: .debug)
                
            case "ShowDetail":
                guard let SetDetailViewController = segue.destination as? SetViewController else {
                    fatalError("Unexpected destination: \(segue.destination)")
                }
                
                guard let selectedSetCell = sender as? SetTableViewCell else {
                    fatalError("Unexpected sender: \(String(describing: sender))")
                }
                
                guard let indexPath = setTableView.indexPath(for: selectedSetCell) else {
                    fatalError("The selected cell is not being displayed by the table")
                }
                
                let selectedSet = setArray[indexPath.row]
                SetDetailViewController.set = selectedSet
                if let exercise = exercise {
                    SetDetailViewController.exerciseName = exercise.name
                }
                
            default:
                fatalError("Unexpected Segue Identifier; \(String(describing: segue.identifier))")
            }
        }
    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        let isPresentingInAddExerciseMode = presentingViewController is UINavigationController
        
        if isPresentingInAddExerciseMode {
            dismiss(animated: true, completion: nil)
        }
        else if let owningNavigationController = navigationController {
            owningNavigationController.popViewController(animated: true)
        }
        else {
            fatalError("The ExerciseViewController is not inside a navigation controller.")
        }
    }
    
    
    //MARK: TableView Functions
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return setArray.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "SetCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? SetTableViewCell else {
            fatalError("The dequeued cell is not an instance of SetTableViewCell.")
        }
        
        let woSet = setArray[indexPath.row]
        
        cell.setDisplayLabel.text = String(woSet.repetition) + " x " + String(woSet.value) + woSet.valueUnitType
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            setArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
        }
    }
    
    func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        let movedSet = setArray[fromIndexPath.row]
        setArray.remove(at: fromIndexPath.row)
        setArray.insert(movedSet, at: to.row)
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    //MARK: Actions
    @IBAction func unwindToSetList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? SetViewController, let set = sourceViewController.set {
            
            if let selectedIndexPath = setTableView.indexPathForSelectedRow {
                // Update an existing set.
                setArray[selectedIndexPath.row] = set
                setTableView.reloadRows(at: [selectedIndexPath], with: .none)
            } else {
                // Add a new set.
                let newIndexPath = IndexPath(row: setArray.count, section: 0)
                
                setArray.append(set)
                setTableView.insertRows(at: [newIndexPath], with: .automatic)
            }
        }
    }
    
    @IBAction func EditClicked(_ sender: UIBarButtonItem) {
        setTableView.setEditing(!setTableView.isEditing, animated: true)
    }
    
}
