//
//  Set.swift
//  WorkoutTracker
//
//  Created by Marc-Antoine Giguère on 2018-04-19.
//  Copyright © 2018 Marc-Antoine Giguère. All rights reserved.
//

import UIKit
import os.log

class WOSet: NSObject, NSCoding {
    
    //MARK: Properties
    var value: Double
    var repetition: Int
    var valueUnitType: String
    
    
    //MARK: Archiving Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("sets")
    
    
    //MARK: Types
    struct PropertyKey {
        static let value = "value"
        static let repetition = "repetition"
        static let valueUnitType = "valueUnitType"
    }
    
    
    //MARK: Initialization
    init?(value: Double, repetition: Int, valueUnitType: String) {
        guard (value >= 0) && (repetition >= 0) else {
            return nil
        }
        
        self.value = value
        self.repetition = repetition
        self.valueUnitType = valueUnitType
    }
    
    
    //MARK: NSCoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(value, forKey: PropertyKey.value)
        aCoder.encode(repetition, forKey: PropertyKey.repetition)
        aCoder.encode(valueUnitType, forKey: PropertyKey.valueUnitType)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let value = aDecoder.decodeDouble(forKey: PropertyKey.value)
        let repetition = aDecoder.decodeInteger(forKey: PropertyKey.repetition)
        guard let valueUnitType = aDecoder.decodeObject(forKey: PropertyKey.valueUnitType) as? String else {
            os_log("Unable to decode the valueUnitType for a WOSet object.", log: OSLog.default, type: .debug)
            return nil
        }
        
        self.init(value: value, repetition: repetition, valueUnitType: valueUnitType)
    }
    
}
