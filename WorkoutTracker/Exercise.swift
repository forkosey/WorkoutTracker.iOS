//
//  Exercise.swift
//  WorkoutTracker
//
//  Created by Marc-Antoine Giguère on 2018-04-19.
//  Copyright © 2018 Marc-Antoine Giguère. All rights reserved.
//

import UIKit
import os.log

class Exercise: NSObject, NSCoding {
    
    //MARK: Properties
    var name: String
    var desc: String?
    var setArray: [WOSet]
    
    
    //MARK: Archiving Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("exercises")
    
    
    //MARK: Types
    struct PropertyKey {
        static let name = "name"
        static let desc = "desc"
        static let setArray = "setArray"
    }
    
    
    //MARK: Initialization
    init?(name: String, desc: String?, setArray: [WOSet]) {
        guard !name.isEmpty else {
            return nil
        }
        
        self.name = name
        self.desc = desc
        self.setArray = setArray
    }
    
    //MARK: NSCoding
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: PropertyKey.name)
        aCoder.encode(desc, forKey: PropertyKey.desc)
        aCoder.encode(setArray, forKey: PropertyKey.setArray)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        guard let name = aDecoder.decodeObject(forKey: PropertyKey.name) as? String else {
            os_log("Unable to decode the name for a Workout object.", log: OSLog.default, type: .debug)
            return nil
        }
        guard let desc = aDecoder.decodeObject(forKey: PropertyKey.desc) as? String else {
            os_log("Unable to decode the desc for a Workout object.", log: OSLog.default, type: .debug)
            return nil
        }
        guard let setArray = aDecoder.decodeObject(forKey: PropertyKey.setArray) as? [WOSet] else {
            os_log("Unable to decode the exerciseArray for a Workout object.", log: OSLog.default, type: .debug)
            return nil
        }
        
        self.init(name: name, desc: desc, setArray: setArray)
    }
    
}
