//
//  SetViewController.swift
//  WorkoutTracker
//
//  Created by Marc-Antoine Giguère on 2018-04-26.
//  Copyright © 2018 Marc-Antoine Giguère. All rights reserved.
//

import UIKit
import os.log
import Floaty

class SetViewController: UIViewController, UITextFieldDelegate {

    //MARK: Properties
    @IBOutlet weak var repetitionTextField: UITextField!
    @IBOutlet weak var valueTextField: UITextField!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var repetitionStepper: UIStepper!
    @IBOutlet weak var valueStepper: UIStepper!
    @IBOutlet weak var valueUnitTypeButton: UIButton!
    @IBOutlet weak var valueLabel: UILabel!
    
    var set: WOSet?
    var exerciseName: String?
    var unitTypeIndex = 0
    
    
    //MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()
        
        repetitionTextField.delegate = self
        repetitionTextField.keyboardType = .decimalPad
        
        valueTextField.delegate = self
        valueTextField.keyboardType = .decimalPad
        
        if let set = set {
            repetitionTextField.text = String(set.repetition)
            valueTextField.text = String(set.value)
            repetitionStepper.value = Double(set.repetition)
            valueStepper.value = set.value
            unitTypeIndex = findValueUnitTypeIndex(set: set)
        }
        if let exerciseName = exerciseName {
            self.title = exerciseName
        }
        
        addButtonBorder()
        refreshValueUnitTypeData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        Floaty.global.hide()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Private Methods
    private func addButtonBorder() {
        valueUnitTypeButton.backgroundColor = .clear
        valueUnitTypeButton.layer.cornerRadius = 5
        valueUnitTypeButton.layer.borderWidth = 1
        valueUnitTypeButton.layer.borderColor = valueUnitTypeButton.titleColor(for: .normal)?.cgColor
    }
    
    private func findValueUnitTypeIndex(set: WOSet) -> Int {
        for (index, valueUnitType) in GlobalUnitType.enumerated() {
            if valueUnitType.1 == set.valueUnitType {
                return index
            }
        }
        return 0
    }
    
    private func refreshValueUnitTypeData() {
        valueUnitTypeButton.setTitle(GlobalUnitType[unitTypeIndex].1, for: .normal)
        valueLabel.text = GlobalUnitType[unitTypeIndex].0
    }
    
    
    //MARK: Delegate Functions
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let invalidCharacters = CharacterSet(charactersIn: "0123456789.").inverted
        return string.rangeOfCharacter(from: invalidCharacters, options: [], range: string.startIndex ..< string.endIndex) == nil
    }
    
    
    //MARK: Actions
    @IBAction func stepperActionRepetition(sender: AnyObject) {
        repetitionTextField.text = String(Int(repetitionStepper.value))
    }
    
    @IBAction func stepperActionWeight(sender: AnyObject) {
        valueTextField.text = String(valueStepper.value)
    }
    
    @IBAction func valueUnitTypeButtonClick(_ sender: Any) {
        if unitTypeIndex + 1 == GlobalUnitType.count {
            unitTypeIndex = 0
        } else {
            unitTypeIndex += 1
        }
        refreshValueUnitTypeData()
    }
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        guard let button = sender as? UIBarButtonItem, button === saveButton else {
            fatalError("Unexpected destination: \(segue.destination)")
        }
        
        var repetition: Int? = Int(repetitionTextField.text!)
        var value: Double? = Double(valueTextField.text!)
        let valueUnitType = GlobalUnitType[unitTypeIndex].1
        
        if repetition == nil {
            repetition = 0
        }
        if value == nil {
            value = 0
        }
        
        set = WOSet.init(value: value!, repetition: repetition!, valueUnitType: valueUnitType)
        
    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        let isPresentingInAddSetMode = presentingViewController is UINavigationController
        
        if isPresentingInAddSetMode {
            dismiss(animated: true, completion: nil)
        }
        else if let owningNavigationController = navigationController {
            owningNavigationController.popViewController(animated: true)
        }
        else {
            fatalError("The SetViewController is not inside a navigation controller.")
        }
    }
 

}
