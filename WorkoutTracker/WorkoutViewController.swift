//
//  WorkoutViewController.swift
//  WorkoutTracker
//
//  Created by Marc-Antoine Giguère on 2018-04-18.
//  Copyright © 2018 Marc-Antoine Giguère. All rights reserved.
//

import UIKit
import os.log
import QuartzCore
import Floaty

class WorkoutViewController: UIViewController, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate {
    
    //MARK: Properties
    @IBOutlet weak var workoutNameTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var FavoriteSwitch: UISwitch!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var exercisesTableView: UITableView!
    @IBOutlet weak var addExerciseButton: UIBarButtonItem!
    
    var workout: Workout?
    var exerciseArray = [Exercise]()
    
    
    //MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Handle the text field’s user input through delegate callbacks.
        workoutNameTextField.delegate = self
        
        exercisesTableView.delegate = self
        exercisesTableView.dataSource = self
        
        // Set up views if editing an existing Workout.
        if let workout = workout {
            workoutNameTextField.text = workout.name
            descriptionTextView.text = workout.desc
            FavoriteSwitch.setOn(workout.isFavorite, animated: false)
            exerciseArray = workout.exerciseArray
            self.title = "Workout"
        }
        
        updateSaveButtonState()
        addTextViewBorder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        Floaty.global.hide()
    }
    
    //MARK: Private Methods
    private func updateSaveButtonState() {
        // Disable the Save button if the text field is empty.
        let text = workoutNameTextField.text ?? ""
        saveButton.isEnabled = !text.isEmpty
    }
    
    private func addTextViewBorder() {
        descriptionTextView.layer.borderWidth = 0.5
        descriptionTextView.layer.borderColor = UIColor.lightGray.cgColor
        descriptionTextView.layer.cornerRadius = 8.0
    }
    
    private func clearSelectedCellIfOneIs(animated: Bool) {
        if let selectedIndexPath = exercisesTableView.indexPathForSelectedRow {
            exercisesTableView.deselectRow(at: selectedIndexPath, animated: animated)
        }
    }
    
    
    //MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Hide the keyboard.
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        saveButton.isEnabled = false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateSaveButtonState()
        navigationItem.title = textField.text
    }
    
    
    //MARK: Navigation
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        let isPresentingInAddWorkoutMode = presentingViewController is UINavigationController
        
        if isPresentingInAddWorkoutMode {
            dismiss(animated: true, completion: nil)
        }
        else if let owningNavigationController = navigationController {
            owningNavigationController.popViewController(animated: true)
        }
        else {
            fatalError("The WorkoutViewController is not inside a navigation controller.")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if let button = sender as? UIBarButtonItem, button === saveButton {
            let name = workoutNameTextField.text ?? ""
            let isFavorite = FavoriteSwitch.isOn
            var description = descriptionTextView.text
            
            if description == "Enter description" {
                description = ""
            }
            
            workout = Workout.init(name: name, desc: description, exerciseArray: exerciseArray, isFavorite: isFavorite)
        } else {
            switch(segue.identifier ?? "") {
            case "AddItem":
                clearSelectedCellIfOneIs(animated: false)
                os_log("Adding a new exercise.", log: OSLog.default, type: .debug)
                
            case "ShowDetail":
                guard let ExerciseDetailViewController = segue.destination as? ExerciseViewController else {
                    fatalError("Unexpected destination: \(segue.destination)")
                }
                
                guard let selectedExerciseCell = sender as? ExerciseTableViewCell else {
                    fatalError("Unexpected sender: \(String(describing: sender))")
                }
                
                guard let indexPath = exercisesTableView.indexPath(for: selectedExerciseCell) else {
                    fatalError("The selected cell is not being displayed by the table")
                }
                
                let selectedExercise = exerciseArray[indexPath.row]
                ExerciseDetailViewController.exercise = selectedExercise
                if let workout = workout {
                    ExerciseDetailViewController.workoutName = workout.name
                }
                
            default:
                fatalError("Unexpected Segue Identifier; \(String(describing: segue.identifier))")
            }
        }
    }
    
    
    //MARK: TableView Functions
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exerciseArray.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "ExerciseCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ExerciseTableViewCell else {
            fatalError("The dequeued cell is not an instance of ExerciseTableViewCell.")
        }
        
        let ex = exerciseArray[indexPath.row]
        
        cell.exerciseNameLabel.text = ex.name
        cell.setNumberLabel.text = String(ex.setArray.count) + " Set(s)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            exerciseArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
        }
    }
    
    func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        let movedExercise = exerciseArray[fromIndexPath.row]
        exerciseArray.remove(at: fromIndexPath.row)
        exerciseArray.insert(movedExercise, at: to.row)
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    //MARK: Actions
    @IBAction func unwindToExerciseList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? ExerciseViewController, let exercise = sourceViewController.exercise {
            
            if let selectedIndexPath = exercisesTableView.indexPathForSelectedRow {
                // Update an existing exercise.
                exerciseArray[selectedIndexPath.row] = exercise
                exercisesTableView.reloadRows(at: [selectedIndexPath], with: .none)
            } else {
                // Add a new exercise.
                let newIndexPath = IndexPath(row: exerciseArray.count, section: 0)
                
                exerciseArray.append(exercise)
                exercisesTableView.insertRows(at: [newIndexPath], with: .automatic)
            }
        }
    }
    
    @IBAction func EditClicked(_ sender: UIBarButtonItem) {
        exercisesTableView.setEditing(!exercisesTableView.isEditing, animated: true)
    }
    
    
}

