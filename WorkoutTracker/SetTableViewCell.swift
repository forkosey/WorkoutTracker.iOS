//
//  SetTableViewCell.swift
//  WorkoutTracker
//
//  Created by Marc-Antoine Giguère on 2018-04-26.
//  Copyright © 2018 Marc-Antoine Giguère. All rights reserved.
//

import UIKit

class SetTableViewCell: UITableViewCell {
    
    //MARK: Properties
    @IBOutlet weak var setDisplayLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
