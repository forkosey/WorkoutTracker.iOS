//
//  WorkoutTrackerTests.swift
//  WorkoutTrackerTests
//
//  Created by Marc-Antoine Giguère on 2018-04-18.
//  Copyright © 2018 Marc-Antoine Giguère. All rights reserved.
//

import XCTest
@testable import WorkoutTracker

class WorkoutTrackerTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    //MARK: Set Class Tests
    func testSetInitializationSucceeds() {
        let normalSet = WOSet.init(value: 100, repetition: 12, valueUnitType: "lbs")
        XCTAssertNotNil(normalSet)
        
        let negativeValueSet = WOSet.init(value: -10, repetition: 12, valueUnitType: "lbs")
        XCTAssertNil(negativeValueSet)
        
        let negativeRepSet = WOSet.init(value: 100, repetition: -12, valueUnitType: "lbs")
        XCTAssertNil(negativeRepSet)
    }
    
    
    //MARK: Exercise Class Tests
    func testExerciseInitializationSucceeds() {
        let normalExercise = Exercise.init(name: "Exercise", desc: "Description", setArray: [WOSet.init(value: 100, repetition: 12, valueUnitType: "lbs")!,
                                                                                             WOSet.init(value: 100, repetition: 12, valueUnitType: "lbs")!])
        XCTAssertNotNil(normalExercise)
    }
    
    func testExerciseWithoutNameFails() {
        let emptyNameExercise = Exercise.init(name: "", desc: "Description", setArray: [WOSet.init(value: 100, repetition: 12, valueUnitType: "lbs")!,
                                                                                        WOSet.init(value: 100, repetition: 12, valueUnitType: "lbs")!])
        XCTAssertNil(emptyNameExercise)
    }
    
    func testExerciseWithNilDescSucceeds() {
        let nilDescExercise = Exercise.init(name: "Exercise", desc: nil, setArray: [WOSet.init(value: 100, repetition: 12, valueUnitType: "lbs")!,
                                                                                    WOSet.init(value: 100, repetition: 12, valueUnitType: "lbs")!])
        XCTAssertNotNil(nilDescExercise)
    }
    
    //MARK: Workout Class Tests
    func testWorkoutInitilizationSucceeds() {
        let normalWO = Workout.init(name: "Workout", desc: "Description", exerciseArray: [
            Exercise.init(name: "Exercise", desc: "Description", setArray: [WOSet.init(value: 100, repetition: 12, valueUnitType: "lbs")!,
                                                                            WOSet.init(value: 100, repetition: 12, valueUnitType: "lbs")!])!,
            Exercise.init(name: "Exercise", desc: "Description", setArray: [WOSet.init(value: 100, repetition: 12, valueUnitType: "lbs")!,
                                                                            WOSet.init(value: 100, repetition: 12, valueUnitType: "lbs")!])!], isFavorite: false)
        XCTAssertNotNil(normalWO)
    }
    
    func testWorkoutWithoutNameFails() {
        let emptyNameWO = Workout.init(name: "", desc: "Description", exerciseArray: [
            Exercise.init(name: "Exercise", desc: "Description", setArray: [WOSet.init(value: 100, repetition: 12, valueUnitType: "lbs")!,
                                                                            WOSet.init(value: 100, repetition: 12, valueUnitType: "lbs")!])!,
            Exercise.init(name: "Exercise", desc: "Description", setArray: [WOSet.init(value: 100, repetition: 12, valueUnitType: "lbs")!,
                                                                            WOSet.init(value: 100, repetition: 12, valueUnitType: "lbs")!])!], isFavorite: false)
        XCTAssertNil(emptyNameWO)
    }
    
    func testWorkoutWithNilDescSucceeds() {
        let nilDescWO = Workout.init(name: "Workout", desc: nil, exerciseArray: [
            Exercise.init(name: "Exercise", desc: "Description", setArray: [WOSet.init(value: 100, repetition: 12, valueUnitType: "lbs")!,
                                                                            WOSet.init(value: 100, repetition: 12, valueUnitType: "lbs")!])!,
            Exercise.init(name: "Exercise", desc: "Description", setArray: [WOSet.init(value: 100, repetition: 12, valueUnitType: "lbs")!,
                                                                            WOSet.init(value: 100, repetition: 12, valueUnitType: "lbs")!])!], isFavorite: false)
        XCTAssertNotNil(nilDescWO)
    }
    
}
